﻿using Assignment.Numbers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    class NumberGeneratorTests
    {
        private NumberGenerator numGenerator;

        [SetUp]
        public void Init()
        {
            numGenerator = new NumberGenerator();
        }

        [TestCase(5)]
        [TestCase(20)]
        [TestCase(25)]
        public void GenerateEven_Should_ReturnEvenNumber(int number)
        {
            int result = numGenerator.GenerateEven(number);

            Assert.That(result % 2 == 0);
        }

        [Test]
        public void GenerateEven_Should_ThrowArgumentOutOfRangException_When_NumberIsMinusOne()
        {
            int number = -1;

            Assert.Throws<ArgumentOutOfRangeException>(() => numGenerator.GenerateEven(number));
        }

        [TestCase(1)]
        [TestCase(10)]
        [TestCase(100)]
        public void GenerateEven_Should_DoesNotThrowException_When_NumberIsHigherMinusOne(int number)
        {
            Assert.DoesNotThrow(() => numGenerator.GenerateEven(number));
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(25)]
        public void GenerateEven_Should_ReturnNumberLessThanLimit(int limit)
        {
            Assert.Less(numGenerator.GenerateEven(limit), limit);
        }

        [TestCase(5)]
        [TestCase(20)]
        [TestCase(25)]
        public void GenerateOdd_Should_ReturnEvenNumber(int number)
        {
            int result = numGenerator.GenerateOdd(number);

            Assert.That(result % 2 == 1);
        }

        [Test]
        public void GenerateOdd_Should_ThrowArgumentOutOfRangException_When_NumberIsZero()
        {
            int number = 0;

            Assert.Throws<ArgumentOutOfRangeException>(() => numGenerator.GenerateOdd(number));
        }

        [TestCase(1)]
        [TestCase(10)]
        [TestCase(100)]
        public void GenerateOdd_Should_DoesNotThrowException_When_NumberIsHigherZero(int number)
        {
            Assert.DoesNotThrow(() => numGenerator.GenerateOdd(number));
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(25)]
        public void GenerateOdd_Should_ReturnNumberLessThanLimit(int limit)
        {
            Assert.Less(numGenerator.GenerateOdd(limit), limit);
        }
    }
}
