﻿using Assignment.Numbers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    class NumberUtilsTest
    {
        private NumberUtils numUtils { get; set; }

        [SetUp]
        public void Init()
        {
            numUtils = new NumberUtils();
        }

        [Test]
        public void GetDivisors_Should_ReturnListOf4Item_When_NumberIs10()
        {
            int number = 10;
            List<int> divisorsWhenNumber10 = new List<int> { 1, 2, 5, 10 };

            List<int> result = numUtils.GetDivisors(number);

            Assert.AreEqual(result.Count(), 4);
            Assert.AreEqual(result, divisorsWhenNumber10);
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        public void IsPrime_Should_ReturnFalse_When_NumberIsPrime(int number)
        {
            Assert.IsTrue(numUtils.IsPrime(number));
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(4)]
        [TestCase(6)]
        public void IsPrime_Should_ReturnTrue_When_NumberIsNotPrime(int number)
        {
            Assert.IsFalse(numUtils.IsPrime(number));
        }

        [TestCase(0)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(8)]
        public void EvenOrOdd_Should_ReturnEven_When_NumberIsEven(int number)
        {
            Assert.AreEqual(numUtils.EvenOrOdd(0), "even");
        }

        [TestCase(1)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        public void EvenOrOdd_Should_ReturnOdd_When_NumberIsOdd(int number)
        {
            Assert.AreEqual(numUtils.EvenOrOdd(1), "odd");
        }
    }
}
