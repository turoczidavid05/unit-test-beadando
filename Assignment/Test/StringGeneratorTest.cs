﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment.Strings;
using Assignment.Numbers;
using Moq;

namespace Test
{
    [TestFixture]
    class StringGeneratorTest
    {
        Mock<INumberGenerator> mockNumGenerator;

        [SetUp]
        public void Init()
        {
            mockNumGenerator = new Mock<INumberGenerator>();
        }

        [Test]
        public void GenerateEvenOddPairs_Should_CallGenerateEvenTwice_When_PairsCountIs2()
        {
            var strGenerator = new StringGenerator(mockNumGenerator.Object);

            int pairCount = 2;
            int max = 20;

            strGenerator.GenerateEvenOddPairs(pairCount, max);

            mockNumGenerator.Verify(x => x.GenerateEven(max), Times.Exactly(2));
        }

        [Test]
        public void GenerateEvenOddPairs_Should_CallGenerateEvenNever_When_PairsCountIsZero()
        {
            var strGenerator = new StringGenerator(mockNumGenerator.Object);

            int pairCount = 0;
            int max = 20;

            strGenerator.GenerateEvenOddPairs(pairCount, max);

            mockNumGenerator.Verify(x => x.GenerateEven(max), Times.Never);
        }

        [TestCase(10, 20)]
        [TestCase(20, 15)]
        [TestCase(30, 70)]
        public void GenerateEvenOddPairs_Should_ReturnedListCountEqualsPairsCount(int pairCount, int max)
        {
            var strGenerator = new StringGenerator(mockNumGenerator.Object);

            List<string> result = strGenerator.GenerateEvenOddPairs(pairCount, max);

            Assert.That(result.Count == pairCount);
        }

        [Test]
        public void GenerateEvenOddPairs_Should_ReturnFirstParamEvenSecondParamOdd()
        {
            var strGenerator = new StringGenerator(mockNumGenerator.Object);
            mockNumGenerator.Setup(x => x.GenerateEven(It.IsAny<int>())).Returns(10);
            mockNumGenerator.Setup(x => x.GenerateOdd(It.IsAny<int>())).Returns(9);

            List<string> result = strGenerator.GenerateEvenOddPairs(20, 20);

            Assert.AreEqual(result[0], "10,9");
        }
    }
}
