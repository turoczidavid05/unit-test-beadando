﻿using Assignment.Strings;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    class StringUtilTest
    {
        private StringUtil strUtil { get; set; }

        [SetUp]
        public void Init()
        {
            strUtil = new StringUtil();
        }

        [Test]
        public void IsPalindrom_Should_ReturnTrue_When_InputStringIsPalindrom()
        {
            string palindrom = "ollo";
            Assert.IsTrue(strUtil.IsPalindrom(palindrom));
        }

        [Test]
        public void IsPalindrom_Should_ReturnFalse_When_InputStringIsNotPalindrom()
        {
            string notPalindrom = "hello";
            Assert.IsFalse(strUtil.IsPalindrom(notPalindrom));
        }
    }
}
